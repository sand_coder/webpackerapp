import { Controller } from 'stimulus'
// Add below line to load jquery if you want add jquery only in this file Or
// For Globle usage of jqyer refer environment.js
// window.$ = window.jQuery = window.jquery = require('jquery')
import 'moment'
import 'fullcalendar'
import 'fullcalendar/dist/fullcalendar.css'

export default class extends Controller{
    static targets = ["calendar"]
    connect(){
        $(this.calendarTarget).fullCalendar({})
    }
}