import { Controller } from 'stimulus'

export default class extends Controller{
    static targets = ["header"]
    connect(){
        console.log("Inside connect")
        this.headerTarget.textContent = "Hello, Stimulus"
    }
    initialize(){
        console.log("Inside initialize")
    }

}