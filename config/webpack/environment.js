const { environment } = require('@rails/webpacker')

// SOF to add jquery gloablly
const webpack = require('webpack')
environment.plugins.prepend(
    'Provide',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
        'window.jquery': 'jquery'
    })
)
// EOF to add jquery gloablly

module.exports = environment
